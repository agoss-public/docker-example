#![feature(proc_macro_hygiene, decl_macro)]
#[macro_use] extern crate rocket;

use log;

#[get("/")]
fn index() -> String {
    log::warn!("/ invoked");
    String::from("Hello World!\n")
}

fn main() {
    env_logger::init();
    log::info!("initializing...");
    rocket::ignite().mount("/", routes![index]).launch();
}
