FROM rust:latest

RUN mkdir /usr/src/demo && \
    useradd -m runuser

WORKDIR /usr/src/demo
COPY src ./src
COPY Cargo.* ./

RUN echo "nameserver 8.8.8.8\nnameserver 1.1.1.1" > /etc/resolv.conf && \
    cat /etc/resolv.conf && \
    rustup default nightly && \
    cargo install --path .

USER runuser
ENTRYPOINT demo