Simple project building a Rust web service into a Docker container.

To build:
- docker build -t demo .

To run:
- docker run --rm -p 8000:8000 --name demo demo

To stop:
- docker kill demo

To launch the rust:latest image:
- docker run -it --entrypoint sh rust:latest

After running a container, to exec a shell IN the container:
- docker exec -it <container uuid or name> sh


----

To build the rust application locally:
    - cargo build # or
    - cargo install --path .

I recommend using '[rustup](https://rustup.rs/)' to manage the rust environment